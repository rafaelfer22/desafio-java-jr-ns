package br.com.desafio.vo;

public class CampanhaVO {

	private String nomeCampanha;
	private Long idTimeCoracao;
	private String dateVigenciaInicio;
	private String dateVigenciaFim;

	public CampanhaVO() {
		// TODO Auto-generated constructor stub
	}

	public CampanhaVO(String nomeCampanha, Long idTimeCoracao, String dateVigenciaInicio, String dateVigenciaFim) {
		super();
		this.nomeCampanha = nomeCampanha;
		this.idTimeCoracao = idTimeCoracao;
		this.dateVigenciaInicio = dateVigenciaInicio;
		this.dateVigenciaFim = dateVigenciaFim;
	}

	public String getNomeCampanha() {
		return nomeCampanha;
	}

	public void setNomeCampanha(String nomeCampanha) {
		this.nomeCampanha = nomeCampanha;
	}

	public Long getIdTimeCoracao() {
		return idTimeCoracao;
	}

	public void setIdTimeCoracao(Long idTimeCoracao) {
		this.idTimeCoracao = idTimeCoracao;
	}

	public String getDateVigenciaInicio() {
		return dateVigenciaInicio;
	}

	public void setDateVigenciaInicio(String dateVigenciaInicio) {
		this.dateVigenciaInicio = dateVigenciaInicio;
	}

	public String getDateVigenciaFim() {
		return dateVigenciaFim;
	}

	public void setDateVigenciaFim(String dateVigenciaFim) {
		this.dateVigenciaFim = dateVigenciaFim;
	}

}

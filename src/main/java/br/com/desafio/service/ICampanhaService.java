package br.com.desafio.service;

import java.util.List;

import br.com.desafio.json.CampanhaJSon;
import br.com.desafio.model.Campanha;
import br.com.desafio.vo.CampanhaVO;

public interface ICampanhaService {

	List<Campanha> saveCampanha(Campanha campanha);

	List<Campanha> deleteCampanha(Long idCampanha);

	Campanha findCampanhaById(Long campanha);

	List<Campanha> findAllCampanahs();

	List<CampanhaVO> convertModelToVO(List<Campanha> campanha);

	CampanhaVO convertModelUniqueCampanhaToVO(Campanha c);
	
	Campanha convertoCampanhaJSonToCampanha(CampanhaJSon cj);

}

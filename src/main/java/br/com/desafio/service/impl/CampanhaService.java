package br.com.desafio.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.desafio.json.CampanhaJSon;
import br.com.desafio.model.Campanha;
import br.com.desafio.repository.CampanhaDao;
import br.com.desafio.service.ICampanhaService;
import br.com.desafio.util.ConvertDates;
import br.com.desafio.vo.CampanhaVO;

@Service
public class CampanhaService implements ICampanhaService {

	@Autowired
	CampanhaDao campanhaDao;

	@Override
	public List<Campanha> saveCampanha(Campanha campanha) {
		
		//Recupera a campanha com Maior data de Vigencia, verifica a regra da nova campanha!
		List<Campanha> cmps = this.findAllCampanahs();
		if (cmps.get(cmps.size()-1).getDataVigenciaFinal().after(campanha.getDataVigenciaFinal())) {
			campanha.setDataVigenciaFinal(cmps.get(cmps.size()-1).getDataVigenciaFinal());
			campanha.getDataVigenciaFinal().add(Calendar.DATE, 1);
		}
		campanhaDao.save(campanha);
		return cmps;
	}

	@Override
	public List<Campanha> deleteCampanha(Long idCampanha) {
		campanhaDao.deleteCampanha(idCampanha);
		return this.findAllCampanahs();
	}

	@Override
	public Campanha findCampanhaById(Long idCampanha) {
		campanhaDao.findById(idCampanha);
		return campanhaDao.findById(idCampanha);
	}

	@Override
	public List<Campanha> findAllCampanahs() {
		return campanhaDao.findAll();
	}

	@Override
	public List<CampanhaVO> convertModelToVO(List<Campanha> campanha) {

		List<CampanhaVO> campanhas = new ArrayList<CampanhaVO>();
		CampanhaVO campanhaVO = null;
		for (Campanha c : campanha) {
			campanhaVO = new CampanhaVO(c.getNomeCampanha(), c.getTimeCoracao(),
					ConvertDates.convertCalendarToString(c.getDataVigenciaInicio()),
					ConvertDates.convertCalendarToString(c.getDataVigenciaFinal()));
			campanhas.add(campanhaVO);
		}
		return campanhas;
	}

	@Override
	public CampanhaVO convertModelUniqueCampanhaToVO(Campanha c) {
		CampanhaVO campanhaVO = new CampanhaVO(c.getNomeCampanha(), c.getTimeCoracao(),
				ConvertDates.convertCalendarToString(c.getDataVigenciaInicio()),
				ConvertDates.convertCalendarToString(c.getDataVigenciaFinal()));
		return campanhaVO;
	}

	@Override
	public Campanha convertoCampanhaJSonToCampanha(CampanhaJSon cj) {

		Campanha c = new Campanha();
		c.setNomeCampanha(cj.getNomeCampanha());
		c.setTimeCoracao(cj.getTimeCoracao());
		c.setDataVigenciaInicio(ConvertDates.convertStringToCalendar(cj.getDataVigenciaInicio()));
		c.setDataVigenciaFinal(ConvertDates.convertStringToCalendar(cj.getDataVigenciaFinal()));
		return c;
	}

}

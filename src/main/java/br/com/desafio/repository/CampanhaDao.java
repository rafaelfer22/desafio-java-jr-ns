package br.com.desafio.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.desafio.model.Campanha;
import br.com.desafio.util.ConvertDates;

@Repository
public class CampanhaDao {

	@PersistenceContext
	private EntityManager manager;
	
	public void save(Campanha campanha){
		manager.persist(campanha);
	}
	
	public Campanha findById(Long idCampanha){
		return manager.find(Campanha.class,idCampanha);
	}
	
	public void deleteCampanha(Long idCampanha){
		Campanha campanha = this.findById(idCampanha);
		manager.remove(campanha);
	}
	
	public List<Campanha> findAll(){
		List<Campanha> campanhas = manager.createQuery("select c from Campanha c where dataVigenciaFinal > :dataAtual order by dataVigenciaFinal",Campanha.class)
				.setParameter("dataAtual",ConvertDates.convertStringToCalendar(ConvertDates.dateAtual())).getResultList();
		return campanhas;
	}
}

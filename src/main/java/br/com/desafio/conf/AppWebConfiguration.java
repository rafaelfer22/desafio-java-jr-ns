package br.com.desafio.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import br.com.desafio.controllers.CampanhaController;
import br.com.desafio.repository.CampanhaDao;
import br.com.desafio.service.impl.CampanhaService;

@EnableWebMvc
@ComponentScan(basePackageClasses={CampanhaController.class, CampanhaService.class, CampanhaDao.class})
public class AppWebConfiguration {
	
	@Bean
	public InternalResourceViewResolver internalResourceViewResolve() {
        InternalResourceViewResolver resolve = new InternalResourceViewResolver();
        resolve.setPrefix("/WEB-INF/views/");
        resolve.setSuffix(".jsp");
        return resolve;
    }	
	
	
	
}

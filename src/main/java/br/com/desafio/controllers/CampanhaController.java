package br.com.desafio.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.desafio.json.CampanhaJSon;
import br.com.desafio.service.ICampanhaService;
import br.com.desafio.vo.CampanhaVO;

@RestController
@Transactional
public class CampanhaController {

	@Autowired
	ICampanhaService campanhaService;

	@RequestMapping(value = "/campanha/", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public List<CampanhaVO> findAllCampanhas() {
		return campanhaService.convertModelToVO(campanhaService.findAllCampanahs());
	}

	@RequestMapping(value = "/campanha/delete/{id}", method = RequestMethod.DELETE, produces = { "application/json" })
	@ResponseBody
	public List<CampanhaVO> deleteCampanhaById(@PathVariable Long id) {
		return campanhaService.convertModelToVO(campanhaService.deleteCampanha(id));
	}

	@RequestMapping(value = "/campanha/find/{id}", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public CampanhaVO findCampanhaById(@PathVariable Long id) {
		return campanhaService.convertModelUniqueCampanhaToVO(campanhaService.findCampanhaById(id));
	}

	@RequestMapping(value = "/campanha/new/", method = RequestMethod.PUT, produces = { "application/json" })
	@ResponseBody
	public List<CampanhaVO> saveCampanha(@RequestBody CampanhaJSon c) {
		c.toString();
		return campanhaService.convertModelToVO(campanhaService.saveCampanha(campanhaService.convertoCampanhaJSonToCampanha(c)));
	
	}

}

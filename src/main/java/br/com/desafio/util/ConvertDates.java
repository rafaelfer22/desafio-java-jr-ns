package br.com.desafio.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ConvertDates {
	
	static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	public static Calendar convertStringToCalendar(String data) {
		try {
			
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(data));
			return c;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String convertCalendarToString(Calendar c){
		return sdf.format(c.getTime());
	}
	
	public static String dateAtual(){
		Date today = Calendar.getInstance().getTime(); 
		String dataAtual = sdf.format(today);
		return dataAtual;
	}
}

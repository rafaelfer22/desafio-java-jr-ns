package br.com.desafio.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Campanha {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCampanha;
	private String NomeCampanha;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataVigenciaInicio;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataVigenciaFinal;
	private Long timeCoracao;

	public Campanha() {
		// TODO Auto-generated constructor stub
	}

	public Campanha(Long idCampanha, String nomeCampanha, Calendar dataVigenciaInicio, Calendar dataVigenciaFinal,
			Long timeCoracao) {
		super();
		this.idCampanha = idCampanha;
		NomeCampanha = nomeCampanha;
		this.dataVigenciaInicio = dataVigenciaInicio;
		this.dataVigenciaFinal = dataVigenciaFinal;
		this.timeCoracao = timeCoracao;
	}

	public Long getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(Long idCampanha) {
		this.idCampanha = idCampanha;
	}

	public String getNomeCampanha() {
		return NomeCampanha;
	}

	public void setNomeCampanha(String nomeCampanha) {
		NomeCampanha = nomeCampanha;
	}

	public Calendar getDataVigenciaInicio() {
		return dataVigenciaInicio;
	}

	public void setDataVigenciaInicio(Calendar dataVigenciaInicio) {
		this.dataVigenciaInicio = dataVigenciaInicio;
	}

	public Calendar getDataVigenciaFinal() {
		return dataVigenciaFinal;
	}

	public void setDataVigenciaFinal(Calendar dataVigenciaFinal) {
		this.dataVigenciaFinal = dataVigenciaFinal;
	}

	public Long getTimeCoracao() {
		return timeCoracao;
	}

	public void setTimeCoracao(Long timeCoracao) {
		this.timeCoracao = timeCoracao;
	}

	@Override
	public String toString() {
		return "Campanha [idCampanha=" + idCampanha + ", NomeCampanha=" + NomeCampanha + ", dataVigenciaInicio="
				+ dataVigenciaInicio + ", dataVigenciaFinal=" + dataVigenciaFinal + ", timeCoracao=" + timeCoracao
				+ "]";
	}

}

package br.com.desafio.json;

public class CampanhaJSon {

	private Long idCampanha;
	private String NomeCampanha;
	private String dataVigenciaInicio;
	private String dataVigenciaFinal;
	private Long timeCoracao;

	public CampanhaJSon() {
		// TODO Auto-generated constructor stub
	}
	
	public CampanhaJSon(Long idCampanha, String nomeCampanha, String dataVigenciaInicio, String dataVigenciaFinal,
			Long timeCoracao) {
		super();
		this.idCampanha = idCampanha;
		NomeCampanha = nomeCampanha;
		this.dataVigenciaInicio = dataVigenciaInicio;
		this.dataVigenciaFinal = dataVigenciaFinal;
		this.timeCoracao = timeCoracao;
	}



	public Long getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(Long idCampanha) {
		this.idCampanha = idCampanha;
	}

	public String getNomeCampanha() {
		return NomeCampanha;
	}

	public void setNomeCampanha(String nomeCampanha) {
		NomeCampanha = nomeCampanha;
	}

	public String getDataVigenciaInicio() {
		return dataVigenciaInicio;
	}

	public void setDataVigenciaInicio(String dataVigenciaInicio) {
		this.dataVigenciaInicio = dataVigenciaInicio;
	}

	public String getDataVigenciaFinal() {
		return dataVigenciaFinal;
	}

	public void setDataVigenciaFinal(String dataVigenciaFinal) {
		this.dataVigenciaFinal = dataVigenciaFinal;
	}

	public Long getTimeCoracao() {
		return timeCoracao;
	}

	public void setTimeCoracao(Long timeCoracao) {
		this.timeCoracao = timeCoracao;
	}

	@Override
	public String toString() {
		return "CampanhaJSon [idCampanha=" + idCampanha + ", NomeCampanha=" + NomeCampanha + ", dataVigenciaInicio="
				+ dataVigenciaInicio + ", dataVigenciaFinal=" + dataVigenciaFinal + ", timeCoracao=" + timeCoracao
				+ "]";
	}
	
	

}

#Configurações.
O pacote esta como .war e é recomendavel que seja deployado no tomcat 8+
É preciso criar um banco de dados chamado "desafio" no mysql, as configurações do Jpa estão no bean do Spring na classe JPAConfiguration.java, a configurações está passando o password do banco como 123

#Observações
Criei apenas o Serviço de Campanhas com todas as regras descritas, infelizmente tive que trabalhar até tarde e me faltou pouco tempo como tive apenas 1 dia para realizar o teste por conta de compromissos profissionais me faltou tempo para me dedicar totalmente ao projeto. Como já é de madrugada e eu preciso acordar daqui a pouco, não conseguirei realizar o segundo serviço. Mas seria basicamente como o primeiro, caso eu seja selicionado posso explicar como faria... mas infelizmente estou com muito sono para programar ele agora, e não gostaria de fazer qualquer coisa, prefiro deixar tudo nos patterns, os testes unitários entram nessa também, se fosse com JUnit até que seria rapido porem eu tenho que usar os testes do Spring para injetar os meus objetos, o mesmo serve para o Exercicío 3. Realmente eu gostaria de trabalhar com vocês, se nos testes vocês já pedem essas coisas que são novas e legais, imagino no dia dia, estou procurando trabalho como desenvolvedor PL mas trabalharia como JR para poder atuar com essas tecnologias.

#DeadLock
O DeadLock ocorre quando uma thread "trava um objeto" e outra thread tenta acessar esse mesmo objeto, se a primeira thread for acessar um segundo objeto que também está travado o sistema entra em deadlock. O ideal nessas ocasiões é utilizar métod wait.

#ParallealStream
Eu estudie um pouco as novas funcionalidades do Java 8, pretendo estudar mais a fundo para tirar a certificação, mas infelizmente não sei as principais diferenças dos metodos ditos, não vou pesquisar na internet e passar um pequeno resumo porque entendo que isso seria trapaça, mas vou aprender.